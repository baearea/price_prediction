"""
Computes relevant metrics given observed data and predicted data.
"""
import numpy as np

class Metrics:
    def __init__(self, observed_data, predicted_data):
        self.y = [x[0] for x in observed_data]
        self.y_pred = predicted_data
        assert (len(self.y) == len(self.y_pred), 
            "Warning: Observed data length != predicted data length")

    def mse(self):
        return np.mean([(x - y)**2 for x, y in zip(self.y,self.y_pred)])

    # Percentage of times the model correctly predicted the direction of the
    # stock.
    def directionAccuracy(self):
        count = 0
        for i in range(1, len(self.y)):
            observed_direction = self.y[i] - self.y[i-1]
            predicted_direction = self.y_pred[i] - self.y_pred[i-1]
            if np.sign(observed_direction) == np.sign(predicted_direction):
                count+=1.0
        return count/len(self.y)
