import tensorflow as tf
import read_data
import datetime
import numpy as np

window_size = 40

statistics = read_data.Statistics()
statistics.populate_statistics_maps()

fb_stock_data = read_data.StockData('FB')
fb_stock_data.populate_stock_data()

data = fb_stock_data.get_rolling_adj_close_data(window_size, normalized=True)

train_percent = .8
train_amount = int(len(data) * train_percent)
train_data = data[:train_amount]
test_data = data[train_amount:]

train_x = [el[:-1] for el in train_data]
train_x = np.reshape(train_x, (len(train_x), -1, 1))
train_y = [[el[-1]] for el in train_data]

test_x = [el[:-1] for el in test_data]
test_x = np.reshape(test_x, (len(test_x), -1, 1))
test_y = [[el[-1]] for el in test_data]

sess = tf.InteractiveSession()

x = tf.placeholder(tf.float32, shape=[None, window_size-1, 1])
y_ = tf.placeholder(tf.float32, shape=[None, 1])

cell = tf.contrib.rnn.LSTMCell(20, state_is_tuple=True)
val, state = tf.nn.dynamic_rnn(cell, x, dtype=tf.float32)
print("val", val)

# Get the final value
val = tf.transpose(val, [1, 0, 2])
last = tf.gather(val, int(val.get_shape()[0]) - 1)

y = tf.contrib.layers.fully_connected(inputs=last, num_outputs=1,
                                      weights_initializer=tf.contrib.layers.xavier_initializer())

loss = tf.reduce_mean((y_ -y)**2)

train_step = tf.train.AdamOptimizer().minimize(loss)

error = tf.reduce_mean(tf.abs(y_ -y))

sess.run(tf.global_variables_initializer())

for i in range(100000):
    x_batch = [train_x[i % len(train_x)]]
    y_batch = [train_y[i % len(train_y)]]

    train_step.run(feed_dict={x: x_batch, y_:y_batch})

    if(i % 1000 == 0):
        print("train error: ", error.eval(feed_dict={x: train_x, y_: train_y}))
        print("test error: ", error.eval(feed_dict={x: test_x, y_: test_y}))

#print(y.eval(feed_dict={x: [[ 214.009998,  214.379993,  210.969995]], y_: [[210.58]]}))