import tensorflow as tf
import split_data
import os

_STOCK_DATA_DIR = './data/stock/train'

files = split_data.get_all_files(_STOCK_DATA_DIR)[:5]

# stock_list_file = os.path.join(_TICKER_DIR, data_list + ".csv")
# ticker_list =  download_data.get_stock_list(stock_list_file)
ticker_list = ['AAPL', 'MSFT', 'AMZN', 'FB', 'T']

file_paths = [os.path.join(_STOCK_DATA_DIR, file) for file in files]

num_steps = 5
batch_size = 10
epoch_size = 10

print(file_paths)

filename_queue = tf.train.string_input_producer(file_paths)

reader = tf.WholeFileReader() #tf.TextLineReader(skip_header_lines=1)
key, value = reader.read(filename_queue)

# Default values, in case of empty columns. Also specifies the type of the
# decoded result.
record_defaults = [[""], [1.0], [1.0]]
_, adj_close_price, _ = tf.decode_csv(
    value, record_defaults=record_defaults)

batch_size = 5
min_after_dequeue = 10
capacity = min_after_dequeue + 3 * batch_size
group = tf.train.batch([adj_close_price], batch_size=batch_size, capacity=capacity)

i = tf.train.range_input_producer(epoch_size, shuffle=False).dequeue()
x = tf.strided_slice(group, [i * num_steps],
                     [(i + 1) * num_steps])

with tf.Session() as sess:
  # Start populating the filename queue.
  coord = tf.train.Coordinator()
  threads = tf.train.start_queue_runners(coord=coord)

  for i in range(12):
    # Retrieve a single instance:
    #print(sess.run([key, value, adj_close_price]))
    print(sess.run(x))

  coord.request_stop()
  coord.join(threads)