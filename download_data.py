"""
Script to download stock price data from pandas_datareader from provided list 
of stock symbols.

Please adjust the following - 
start_date in download_stock_data()
columns in download_stock_data() 
stock_list_file in main()

TODO 
only 'Adj Close' accounts for splits and reverse splits as of now
"""

from pandas import DataFrame as data_frame

import csv
import datetime
import os
import pandas_datareader.data as web

# formats
_CSV = '.csv'

# names
_SOURCE = 'yahoo'
_STATISTICS = 'statistics'

# directories
_DATA_DIR = './data/'
_MODELS_DATA_DIR = './data/models/'
_STATISTICS_DATA_DIR = './data/statistics/'
_STOCK_DATA_DIR = './data/stock/'
_TICKER_DIR = 'ticker_lists'

# ticker lists
_SP500_LIST = 'sp500'
_TECH30_LIST = 'tech30'
_TEST5_LIST = 'test5'
_TEST = '_test'

def download_stock_data(ticker, debug=False):
  # historical date when oldest S&P 500 stock ticker was listed
  start_date = (
    datetime.datetime(2017, 1, 1) if debug else datetime.datetime(1964, 1, 1))
  end_date = datetime.datetime(2017, 2, 23)
  stock_data_frame = web.DataReader(ticker, _SOURCE, start_date, end_date)

  # TODO: account for stock split and reverse split
  # Adjusted Close accounts for that so we include that for now
  columns = ['Adj Close', 'Volume']
  # columns = ['Open', 'High', 'Low', 'Close', 'Volume', 'Adj Close']

  statistics_file_suffix = (
    _STATISTICS + _TEST + _CSV if debug else _STATISTICS + _CSV)
  stock_file_suffix = ticker + _TEST + _CSV if debug else ticker + _CSV

  statistics_file_name = os.path.join(
    _STATISTICS_DATA_DIR, statistics_file_suffix)
  stock_file_name = os.path.join(_STOCK_DATA_DIR, stock_file_suffix)

  for column in columns:
    write_statistics_to_csv(
      ticker, column, stock_data_frame[column], statistics_file_name)
  write_data_frame_to_csv(stock_file_name, stock_data_frame, columns)

def write_data_frame_to_csv(file_name, data_frame, columns):
  with open(file_name, 'w') as stock_file:
    data_frame.to_csv(stock_file, columns=columns)

def write_statistics_to_csv(ticker, column, data_series, file_name):
  max = data_series.max()
  min = data_series.min()
  with open(file_name, 'a') as statistics_file:
    writer = csv.writer(statistics_file, delimiter=',')
    writer.writerow([ticker, column, 'max', max, 'min', min])

def get_stock_list(file_name):
  with open(file_name, 'r') as in_file:
    return [ticker.strip("\n") for ticker in in_file.readlines()]

def download_stock_list(file_name, debug):
    for ticker in get_stock_list(file_name):
      print('downloading ', ticker)
      download_stock_data(ticker, debug=debug)

def create_directories(directory_list):
  for directory in directory_list:
    if not os.path.exists(directory):
      os.makedirs(directory)

# TESTS

def test_create_directories():
  dirs = [_DATA_DIR, _MODELS_DATA_DIR, _STOCK_DATA_DIR, _STATISTICS_DATA_DIR]
  create_directories(dirs)

def test_download_stock_list(tech_list, debug):
  # lists = [_TEST5_LIST, _TECH30_LIST, _SP500_LIST]
  stock_list_file = os.path.join(_TICKER_DIR, tech_list + _CSV)
  download_stock_list(stock_list_file, debug)

if __name__ == '__main__':
  # test_create_directories()
  test_download_stock_list(_TEST5_LIST, debug=False)
