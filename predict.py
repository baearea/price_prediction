import numpy as np

def predict(model, test_x):
    predictions = []
    for i in range(len(test_x)):
        predictions.append(model.predict([test_x[i]])[0][0])
    return predictions

def predict_series(model,x,  x_start, y):
    predictions = []
    for i in range(len(y)):
        if i == 0:
            x_input = x_start
        else:
            x_input = np.concatenate((x_start[1:], pred[0]))
        pred = model.eval(feed_dict={x: [x_input]})
        predictions.append(pred[0][0])
    return predictions

