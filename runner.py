import datetime
import metrics
import models
import numpy as np
import plot
import predict
import random
import read_data
import tensorflow as tf
import time
import os
import download_data
import models2

# initialize parameters
_WINDOW_SIZE = 40

# number of stocks to read at a time for training
_TICKER_LIST_BATCH_SIZE = 10

# number of training points to use at a time for training
_TRAIN_BATCH_SIZE = 10

# number of batches of stocks to use for training. Program will stop after 
# training on these number of batches even if there are more available
_BATCHES_TO_RUN_UNTIL = 10

# L2 regularization hyper parameter
_BETA = 0.01

# name of directory where stock data is stored
_TICKER_DIR = 'ticker_lists'

# name of directory where models are saved
_MODEL_DIR = "data/models/"

# checkpoint suffix of saved models
_MODEL_CHECKPOINT_SUFFIX = ".ckpt"

# compilation of stocks to run training on
_SP500_LIST = 'sp500'
_TECH_30_LIST = 'tech30'
_TEST5_LIST = 'test5'

# list of stocks to use below
data_list = _TECH_30_LIST

def get_model_path():
    model_name = data_list + "_" + time.strftime("%m-%d-%y_%H-%M")
    return os.path.join(_MODEL_DIR, model_name + _MODEL_CHECKPOINT_SUFFIX)

def get_ticker_list_in_batches(ticker_list, batch_size=10):
    return [ticker_list[i:i + batch_size] for i in range(
        0, len(ticker_list), batch_size)]

def get_evaluation_ticker_set(ticker_list, sample_size=10):
    # get random sample of stock tickers to evaluate on
    return np.random.choice(ticker_list, sample_size).tolist()

# setup tensorflow
sess = tf.InteractiveSession()

model = models2.Lstm(_WINDOW_SIZE - 1, _BETA)

sess.run(tf.global_variables_initializer())
try:
    saver = tf.train.Saver()
except:
    pass

model_path = get_model_path()

# setup batch training of stocks

stock_list_file = os.path.join(_TICKER_DIR, data_list + ".csv")
ticker_list =  download_data.get_stock_list(stock_list_file)
# ticker_list = ['AAPL', 'MSFT', 'AMZN', 'FB', 'T']
batch_ticker_lists = get_ticker_list_in_batches(
    ticker_list, _TICKER_LIST_BATCH_SIZE)

len_batch_ticker_lists = len(batch_ticker_lists)

for index, ticker_batch in enumerate(batch_ticker_lists):
    print('training ', index, ' of ', len_batch_ticker_lists, ' batches')
    print('training on list ', ticker_batch)

    # populate train and validation stocks
    train_stocks = read_data.Stocks(
        ticker_batch, read_data.DataType.TRAIN, debug=False)
    validate_stocks = read_data.Stocks(
        ticker_batch, read_data.DataType.VALIDATION, debug=False)

    # setup train and test data
    train_data = train_stocks.get_aggregated_rolling_data(
        _WINDOW_SIZE, normalized=True)
    validation_data = validate_stocks.get_aggregated_rolling_data(
        _WINDOW_SIZE, normalized=True)

    train_x = [el[:-1] for el in train_data]
    train_y = [[el[-1]] for el in train_data]

    validation_x = [el[:-1] for el in validation_data]
    validation_y = [[el[-1]] for el in validation_data]

    train_error_list = []
    validation_error_list = []

    # train model
    # train over all batches in train set twice
    for i in range((len(train_x) // _TRAIN_BATCH_SIZE) * 2):
        x_batch = []
        y_batch = []
        for j in range(_TRAIN_BATCH_SIZE):
            pos = random.randint(0, len(train_x)-1)
            x_batch.append(train_x[pos])
            y_batch.append(train_y[pos])

        model.train(x_batch, y_batch)

        if (i % 1000 == 0):
            # compute train and test error
            train_error = model.error(train_x, train_y)
            validation_error = model.error(validation_x, validation_y)
            train_error_list.append(train_error)
            validation_error_list.append(validation_error)
            print("train error: ", train_error)
            print("validation error: ", validation_error)

            # save model
            try:
                saver.save(sess, model_path)
                print ("Saved model weights to ", model_path)
            except:
                pass

    # plot graph for last set of training data
    if (index == len_batch_ticker_lists - 1 or 
        index == _BATCHES_TO_RUN_UNTIL - 1):
        predicted_data = predict.predict(model, validation_x)
        test_data = [x[0] for x in validation_y]
        met  = metrics.Metrics(validation_y, predicted_data)
        print("MSE", met.mse())
        print("Direction Accuracy", met.directionAccuracy())

        # Plot actual price vs predicted price
        plt_prediction = plot.Plot(
            data1=test_data, data2=predicted_data, xlabel='iteration',
            ylabel='normalized price', data1_name='observed',
            data2_name='predicted')
        plt_prediction.simple_plot()

        # Plot train error vs test error
        plt_error = plot.Plot(
            data1=train_error_list, data2=validation_error_list,
            xlabel='iteration', ylabel='error', data1_name='train error',
            data2_name='test error')
        plt_error.simple_plot()

        plt_prediction.show_plot()
        plt_error.show_plot()
        break
