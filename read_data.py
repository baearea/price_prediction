"""
Script to read Stock Data and Statistics Data.
"""
from enum import Enum

import numpy as np
import pandas as pd
import os

_ADJ_CLOSE = 'Adj Close'
_DATE = 'Date'

_CSV = '.csv'
_TEST = '_test'
_STATISTICS = 'statistics'

_MAX = 'max'
_MIN = 'min'

_STOCK_DATA_DIR = './data/stock/'
_STATISTICS_DATA_DIR = './data/statistics/'

_TRAIN_DIR = 'train/'
_TEST_DIR = 'test/'
_VALIDATION_DIR = 'validation/'

def rolling_window(a, window_len):
    shape = a.shape[:-1] + (a.shape[-1] - window_len + 1, window_len)
    strides = a.strides + (a.strides[-1],)
    try:
      return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    except ValueError:
      return None

def normalize_data_point(value, max, min):
  return 2 * (value - min) / (max - min) - 1

def get_data_type_dir_suffix(data_type):
  if data_type == DataType.TRAIN:
    return _TRAIN_DIR
  if data_type == DataType.VALIDATION:
    return _VALIDATION_DIR
  if data_type == DataType.TEST:
    return _TEST_DIR

class DataType(Enum):
  TRAIN = 1
  VALIDATION = 2
  TEST = 3

class StockData:
  """
  Class to populate train, validate or test data for a given stock ticker.
  """
  def __init__(self, ticker, data_type, debug=False):
    self._ticker = ticker
    self._data_type = data_type
    self._adj_close_series = None
    self._date_series = None
    self._adj_close_max = None
    self._adj_close_min = None

    data_file_suffix = (
      self._ticker + _TEST + _CSV if debug else self._ticker + _CSV)
    data_type_suffix = get_data_type_dir_suffix(data_type)
    self._data_file = os.path.join(
      _STOCK_DATA_DIR, data_type_suffix, data_file_suffix)

    self.populate_stock_data()

  def populate_stock_data(self):
    data_frame = pd.read_csv(self._data_file)
    self._adj_close_series = data_frame[_ADJ_CLOSE]
    self._date_series = data_frame[_DATE]
    self._adj_close_max = self._adj_close_series.max()
    self._adj_close_min = self._adj_close_series.min()
    # normalize data
    self._normalized_adj_close_series = self._adj_close_series.apply(
      normalize_data_point, args=(self._adj_close_max, self._adj_close_min))

  def get_rolling_adj_close_data(self, window_len, normalized=False):
    series = self._get_adj_close_series(normalized)
    return rolling_window(series, window_len)

  def get_adj_close_data(self, normalized=False):
    series = self._get_adj_close_series(normalized)
    return series

  # HELPER METHODS
  def _get_adj_close_series(self, normalized=False):
    return (self._normalized_adj_close_series if normalized 
      else self._adj_close_series)

class Stocks:
  """
  Class to populate multiple stocks and get data.
  """
  def __init__(self, ticker_list, data_type, debug=False):
    self._ticker_list = ticker_list
    self._debug = debug
    self._data_type = data_type
    self._stock_data_map = {}
    self.populate_stocks()

  def populate_stocks(self):
    for ticker in self._ticker_list:
      stock_data = StockData(ticker, self._data_type, self._debug)
      stock_data.populate_stock_data()
      self._stock_data_map[ticker] = stock_data

  def get_aggregated_rolling_data(self, window_len, normalized=False):
    data = None
    for ticker in self._ticker_list:
      stock_data = self._stock_data_map[ticker]
      temp_data = stock_data.get_rolling_adj_close_data(window_len, normalized)
      if temp_data is None:
        continue
      data = temp_data if data is None else np.concatenate((data, temp_data))
    return data

  def get_aggregated_data(self, normalized=False):
    data = None
    for ticker in self._ticker_list:
      stock_data = self._stock_data_map[ticker]
      temp_data = stock_data.get_adj_close_data(normalized)
      data = temp_data if data is None else np.concatenate((data, temp_data))
    return data

class Statistics:
  """
  Class to populate and get aggregate statistics of all stock tickers.
  """
  def __init__(self):
    self._adj_close_stats_map = {}
    self._statistics_file = os.path.join(
      _STATISTICS_DATA_DIR, _STATISTICS + _CSV)

  def populate_statistics_maps(self,):
    with open(self._statistics_file) as in_file:
      for line in in_file.readlines():
        elements = line.split(',')
        temp_dict = {}
        temp_dict[_MAX] = float(elements[3].strip())
        temp_dict[_MIN] = float(elements[5].strip())

        if elements[1] == _ADJ_CLOSE:
          self._adj_close_stats_map[elements[0]] = temp_dict

def test_statistics():
  statistics = Statistics()
  statistics.populate_statistics_maps()
  print(statistics._adj_close_stats_map)

def test_stock_data():
  stock = StockData('FB', DataType.TRAIN, debug=True)
  print(stock.get_adj_close_data(normalized=False))
  print(stock.get_rolling_adj_close_data(4, normalized=True))

def test_stocks():
  ticker_list = ['AAPL', 'MSFT']
  stocks = Stocks(ticker_list, DataType.TRAIN, debug=True)
  print(stocks.get_aggregated_rolling_data(4))

if __name__ == '__main__':
  # test_statistics()
  # test_stock_data()
  # test_stocks()
  pass
