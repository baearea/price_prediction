import tensorflow as tf
import numpy as np

class MultilayerPerceptron:
    def __init__(self, n_input, beta):
        self.n_hidden_1 = 40
        self.n_hidden_2 = 40
        self.n_input = n_input
        self.beta = beta
        self.weights = {
            'h1': tf.Variable(tf.random_normal([n_input, self.n_hidden_1])),
            'h2': tf.Variable(tf.random_normal([self.n_hidden_1, self.n_hidden_2])),
            'out': tf.Variable(tf.random_normal([self.n_hidden_2, 1]))
        }
        self.biases = {
            'b1': tf.Variable(tf.random_normal([self.n_hidden_1])),
            'b2': tf.Variable(tf.random_normal([self.n_hidden_2])),
            'out': tf.Variable(tf.random_normal([1]))
        }
        self.x = tf.placeholder(tf.float32, shape=[None, n_input])
        self.y_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y = self._model()

        self.train_step_val = self._train_step()

    def _model(self):
        layer1 = tf.add(tf.matmul(self.x, self.weights['h1']), self.biases['b1'])
        layer1 = tf.nn.relu(layer1)
        layer2 = tf.add(tf.matmul(layer1, self.weights['h2']), self.biases['b2'])
        layer2 = tf.nn.relu(layer2)
        # output layer
        out_layer = tf.matmul(layer2, self.weights['out']) + self.biases['out']
        return out_layer

    def _loss(self):
        regularizers = (
            tf.nn.l2_loss(self.weights['h1']) + tf.nn.l2_loss(self.weights['h2']
                                                         + tf.nn.l2_loss(self.weights['out'])))
        return tf.reduce_mean((self.y_ - self.y) ** 2 + self.beta * regularizers)

    def _train_step(self):
        return tf.train.AdamOptimizer().minimize(self._loss())

    def error(self, x_batch, y_batch):
        """Used for evaluation."""
        tf_error = tf.reduce_mean(tf.abs(self.y_ - self.y))
        return tf_error.eval(
                feed_dict={self.x: x_batch, self.y_: y_batch})

    def train(self, x_batch, y_batch):
        return self.train_step_val.run(feed_dict={self.x: x_batch, self.y_: y_batch})

    def predict(self, x_batch):
        return self.y.eval(
                feed_dict={self.x: x_batch})

class Lstm:
    def __init__(self, n_input, beta):
        self.n_input = n_input

        self.x = tf.placeholder(tf.float32, shape=[None, n_input])
        self.y_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y = self._model()

        self.train_step_val = self._train_step()

    def _model(self):
        x = tf.reshape(self.x, [-1, self.x.get_shape().as_list()[1], 1])
        cell = tf.contrib.rnn.LSTMCell(2, state_is_tuple=True)
        cell = tf.contrib.rnn.MultiRNNCell([cell] * 1, state_is_tuple=True)
        val, state = tf.nn.dynamic_rnn(cell, x, dtype=tf.float32)
        print("val", val)

        # Get the final value
        val = tf.transpose(val, [1, 0, 2])
        last = tf.gather(val, int(val.get_shape()[0]) - 1)

        return tf.contrib.layers.fully_connected(
            inputs=last, num_outputs=1,
            weights_initializer=tf.contrib.layers.xavier_initializer(),
            activation_fn=None)

    def _loss(self):
        return tf.reduce_mean((self.y_ - self.y) ** 2)

    def _train_step(self):
        return tf.train.AdamOptimizer().minimize(self._loss())

    def error(self, x_batch, y_batch):
        """Used for evaluation."""
        tf_error = tf.reduce_mean(tf.abs(self.y_ - self.y))
        return tf_error.eval(
            feed_dict={self.x: x_batch, self.y_: y_batch})

    def train(self, x_batch, y_batch):
        return self.train_step_val.run(feed_dict={self.x: x_batch, self.y_: y_batch})

    def predict(self, x_batch):
        return self.y.eval(
            feed_dict={self.x: x_batch})

class Dumb:
    def __init__(self, n_input, beta):
        pass

    def error(self, x_batch, y_batch):
        return np.mean(np.abs(np.array(y_batch) - np.array(self.predict(x_batch))))

    def train(self, x_batch, y_batch):
        pass

    def predict(self, x_batch):
        return [[x[-1]] for x in x_batch]