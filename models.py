import tensorflow as tf

# LSTM

def lstm_model(x):
    x = tf.reshape(x, [-1, x.get_shape().as_list()[1], 1])
    cell = tf.contrib.rnn.LSTMCell(20, state_is_tuple=True)
    cell = tf.contrib.rnn.MultiRNNCell([cell] * 3, state_is_tuple=True)
    val, state = tf.nn.dynamic_rnn(cell, x, dtype=tf.float32)
    print("val", val)

    # Get the final value
    val = tf.transpose(val, [1, 0, 2])
    last = tf.gather(val, int(val.get_shape()[0]) - 1)

    return tf.contrib.layers.fully_connected(
    	inputs=last, num_outputs=1,
    	weights_initializer=tf.contrib.layers.xavier_initializer(),
        activation_fn=None)

# Percepteron

def multilayer_perceptron(x, weights, biases, tf):
    layer1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    layer1 = tf.nn.relu(layer1)
    layer2 = tf.add(tf.matmul(layer1, weights['h2']), biases['b2'])
    layer2 = tf.nn.relu(layer2)
    # output layer
    out_layer = tf.matmul(layer2, weights['out']) + biases['out']
    return out_layer

def percepteron_model(x, tf, n_input):
    n_hidden_1 = 40
    n_hidden_2 = 40
    weights = {
           'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
           'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
           'out': tf.Variable(tf.random_normal([n_hidden_2, 1]))
           }
    biases = {
                'b1': tf.Variable(tf.random_normal([n_hidden_1])),
                'b2': tf.Variable(tf.random_normal([n_hidden_2])),
                'out': tf.Variable(tf.random_normal([1]))
                }
    return multilayer_perceptron(x, weights, biases, tf), weights
