"""
Take downloaded .csv files in data/stock and split them into train, test and validation data under data/stock/train/, data/stock/test/ and data/stock/split/.

The split proportion is 70 : 20 : 10 (train : validation : test).
"""

from pandas import DataFrame as data_frame
import csv
import download_data
import os
import re

_STOCK_DATA_DIR = './data/stock/'
_TRAIN_DATA_DIR = os.path.join(_STOCK_DATA_DIR, 'train')
_VALIDATION_DATA_DIR = os.path.join(_STOCK_DATA_DIR, 'validation')
_TEST_DATA_DIR = os.path.join(_STOCK_DATA_DIR, 'test')

_TRAIN_RATIO = .70
_VALIDATION_RATI0 = .20
_TEST_RATIO = .10

# iterate over csv files in ./data/stock and populate them in the respective
# directories.
def get_all_files(dir):
	all_files = next(os.walk(dir))[2]
	regex = re.compile('.csv')
	return list(filter(lambda f : regex.search(f), all_files))

def write_data_to_csv(file_name, header, data_list):
	with open(file_name, 'w') as out_file:
		out_file.write(header + '\n')
		for line in data_list:
			out_file.write(line + '\n')

# ensure that the directories are created
# it is important for ordering of the directory_list to be TRAIN, VALIDATE, TEST
directory_list = [_TRAIN_DATA_DIR, _VALIDATION_DATA_DIR, _TEST_DATA_DIR]
download_data.create_directories(directory_list)

files = get_all_files(_STOCK_DATA_DIR)

for file in files:
	with open(os.path.join(_STOCK_DATA_DIR, file), 'r') as f:
		data = f.read().split('\n')
		header = data.pop(0)
		train_limit = int(_TRAIN_RATIO * len(data))
		validate_limit = int((_VALIDATION_RATI0 + _TRAIN_RATIO) * len(data))

		# write split data to respective files
		write_data_to_csv(
			os.path.join(_TRAIN_DATA_DIR, file), header, data[:train_limit])
		write_data_to_csv(
			os.path.join(_VALIDATION_DATA_DIR, file), header, 
			data[train_limit:validate_limit])
		write_data_to_csv(
			os.path.join(_TEST_DATA_DIR, file), header, data[validate_limit:])
