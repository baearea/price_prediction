"""
Plot the observed data vs predicted data
"""
import datetime as dt 
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

class Plot:
	def __init__(self, data1=[], data2=[], xlabel='', ylabel='', data1_name='', data2_name=''):
		self.y = data1
		self.y_hat = data2
		self.xlabel = xlabel
		self.ylabel = ylabel
		self.data1_name = data1_name
		self.data2_name = data2_name

		if len(self.y) != len(self.y_hat):
			print("Warning: Observed data length does not equal to predicted data length")
	
	def simple_plot(self):
		plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')

		line_y_hat, = plt.plot(self.y_hat,'r', label='Predicted Data')
		line_y, = plt.plot(self.y,'b', label='Observed Data')
		plt.legend([line_y,line_y_hat],[self.data1_name, self.data2_name])
		plt.xlabel(self.xlabel)
		plt.ylabel(self.ylabel)
		title = self.data1_name + ' vs ' + self.data2_name
		plt.title(title)

	def show_plot(self):
		plt.show()

def test_plot():
	plt = Plot(data1=[1, 2, 3], data2=[4, 5, 6])
	plt.simple_plot()
	plt.show_plot()

if __name__ == '__main__':
	test_plot()
